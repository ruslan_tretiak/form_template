(function($) {

    $('#submit').click( function() {
        var form = $('#form_apps');
        var data = form.serializeArray();
        var result = true;

        $.each($(form).find('input'), function (key, input) {
            if('' === $(input).val()){
                $(input).css("border", "1px solid red");
                result = false;
            }else {
                $(input).css("border", "1px solid #ebebeb");
            }
        });

        if(result) {
            $.ajax({
                type: 'POST',
                url: 'https://air2.yaroslav-samoylov.com/lead/zoho/',
                dataType: 'json',
                data: data,
                beforeSend: function (data) {
                    startLoader('Идет отправка данных...');
                },
                success: function (data) {
                    window.location.href = data['redirect'];
                },
                error: function (data) {
                    stopLoader();
                    alert('Возникла ошибка, заполните, пожалуйста, форму корректными данными и попробуйте еще раз!');
                }
            })
        }
    })

    function startLoader(text){
        var loader = $('.loaders');
        $(loader).find('.preloader').append('\n' +
            '        <img style="margin-left: 10%;" src="./images/ww.svg">' +
            '<div class="loader_text">'+text+'</div>');
        $(loader).addClass('preloader-wrapper');
    }

    function stopLoader(){
        var loader = $('.loaders');
        $('.loaders').removeClass('loaders preloader-wrapper').addClass('loaders ');
        $(loader).find('.preloader').empty();
    }

})(jQuery);

